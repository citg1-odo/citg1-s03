package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class Calculator extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4271812003605955545L;
	
	public void init() throws ServletException{
		System.out.println("Servlet initiated.");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app!</h1>");
		out.println("<p>To use the app, input two numbers and an operation.</p>");
		out.println("<p>Hit the submit button after filling in the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		char operation = req.getParameter("operation").charAt(0);
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int ans;
		String operations = "";
		
		if(operation == '+') {
			ans = num1 + num2;
			operations = "add";
		}else if(operation == '-') {
			ans = num1 - num2;
			operations = "minus";
		}else if(operation == '*') {
			ans = num1 * num2;
			operations = "multiply";
		}else {
			ans = num1 / num2;
			operations = "divide";
		}
		
		PrintWriter out = res.getWriter();
		out.println("<p>The two numbers you provided are: " +num1+ ", " +num2+ "</p");
		out.println("<p>The operation that you wanted is: " +operations+ "</p>");
		out.println("<p>The result is: " +ans+ "</p>");
	}
	
	public void destroy() {
		System.out.println("Servlet destroyed.");
	}

}
